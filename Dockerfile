FROM alpine:3.13

RUN apk add --no-cache duplicity \
                       py3-boto3 \
                       iproute2

ADD backup.sh /etc/periodic/hourly/backup
RUN chmod +x /etc/periodic/hourly/backup

ENTRYPOINT ["/usr/sbin/crond", "-f"]
