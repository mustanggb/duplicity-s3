#!/bin/sh

# Do nothing if backup is already running
if [ -z "$(pgrep -f /etc/periodic/hourly/backup)" ]; then
  exit
fi

# Throttle upload speed (requires NET_ADMIN)
if [ -n "${MAX_UPLOAD_SPEED}" ]; then
  hz=$(getconf CLK_TCK)
  burst=$((${MAX_UPLOAD_SPEED} * 250 / ${hz}))
  tc qdisc replace dev eth0 root tbf rate ${MAX_UPLOAD_SPEED}mbit burst ${burst}kb latency 500ms
fi

# Create weekly full backups and hourly incremental backups
duplicity --full-if-older-than 1W /data boto3+s3://$BUCKET

# Remove backups old than a month
duplicity remove-older-than 1M --force boto3+s3://$BUCKET

# Cleanup dangling files from incomplete backups
duplicity cleanup --force boto3+s3://$BUCKET
